const path = require("path");

// const fs = require("fs");
// const files = fs.readdirSync("./pages", { withFileTypes: true });
// console.log(files);
// console.log(files[1].isDirectory());

const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    index: path.resolve(__dirname, "src", "index.js"),
  },
  module: {
    rules: [
      {
        test: /\.html$/i,
        loader: "html-loader",
      },
      {
        test: /\.s[ca]ss$/i,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
    ],
  },
  output: {
    clean: true,
    filename: "[name].js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      chunks: ["index"],
      template: path.resolve(__dirname, "src", "index.html"),
    }),
    new HtmlWebpackPlugin({
      filename: "pages/01-login/login.html",
      chunks: ["index"],
      template: path.resolve(__dirname, "pages", "01-login", "login.html"),
    }),
    new HtmlWebpackPlugin({
      filename: "pages/03-password-recovery/password-recovery-sent.html",
      chunks: ["index"],
      template: path.resolve(
        __dirname,
        "pages",
        "03-password-recovery",
        "password-recovery-sent.html"
      ),
    }),
    new HtmlWebpackPlugin({
      filename: "pages/03-password-recovery/password-recovery.html",
      chunks: ["index"],
      template: path.resolve(
        __dirname,
        "pages",
        "03-password-recovery",
        "password-recovery.html"
      ),
    }),
    new HtmlWebpackPlugin({
      filename: "pages/04-new-password/new-password.html",
      chunks: ["index"],
      template: path.resolve(
        __dirname,
        "pages",
        "04-new-password",
        "new-password.html"
      ),
    }),
  ],
};
