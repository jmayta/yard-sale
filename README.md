
# Yard Sale
## A Web Development Challenge

This is an exercise from Platzi's [Frontend Developer Practice Course](https://platzi.com/cursos/frontend-developer-practico/). I took it like a challenge, first I completed the course taking and aproving the certification questionary; last challenge is get the final product without see the course videos (at least until I get stuck). These are the prototype links and style guide for the final product.

### 🔗 Style Sources Links:

+ [Style Guide](https://scene.zeplin.io/project/60afeeed20af1378ed046538)
+ [Mobile Mockup](https://www.figma.com/proto/bcEVujIzJj5PNIWwF9pP2w/Platzi_YardSale?page-id=0%3A1&node-id=0%3A719&starting-point-node-id=0%3A719)
+ [Desktop Mockup](https://www.figma.com/proto/bcEVujIzJj5PNIWwF9pP2w/Platzi_YardSale?node-id=3%3A2112&amp;scaling=scale-down&amp;page-id=0%3A998&amp;starting-point-node-id=5%3A2808)
